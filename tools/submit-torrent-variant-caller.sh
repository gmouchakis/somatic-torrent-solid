#!/bin/sh

curl -v "http://83.212.73.203:31071/api/workflows/v1"\
      -F workflowUrl=https://gitlab.precmed.iit.demokritos.gr/sgsfak/ion-torrent-pipeline/raw/master/tools/torrent_variant_caller.cwl \
      -F workflowInputs=@torrent_variant_caller-inputs.yml \
      -F workflowType=CWL \
      -F workflowTypeVersion=v1.0
