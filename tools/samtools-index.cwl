#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
id: samtools-index

hints:
  - class: DockerRequirement
    dockerPull: sgsfak/tmap-tvc

requirements:
  InitialWorkDirRequirement:
    listing: [ $(inputs.alignments) ]

inputs:
  - id: alignments
    type: File
    inputBinding:
      position: 0
    label: Input bam file

baseCommand: [samtools, index, -b]

outputs:
  - id: alignments_with_index
    type: File
    outputBinding:
      glob: $(inputs.alignments.basename)
    secondaryFiles:
      - .bai
    doc: The index file

