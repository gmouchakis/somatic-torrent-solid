#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "Usage: `basename $0` <workflowSource> <workflowInputsFile>"
    exit 1
fi

SRCFILE=$1
INPUT_FILE=$2


## http://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0

SERVER=${CROMWELL_SERVER:-http://cromwell.testbed-precmed.iit.demokritos.gr}
>&2 echo "${red}Using Cromwell at $SERVER..${reset}"

curl -s "$SERVER/api/workflows/v1"\
      -F workflowSource=@$SRCFILE \
      -F workflowInputs=@$INPUT_FILE \
      -F workflowType=CWL \
      -F workflowTypeVersion=v1.0 | jq -r .id
